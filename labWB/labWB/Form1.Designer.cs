﻿namespace labWB
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.edURL = new System.Windows.Forms.TextBox();
            this.ButGO = new System.Windows.Forms.Button();
            this.ButBack = new System.Windows.Forms.Button();
            this.ButForward = new System.Windows.Forms.Button();
            this.ButReload = new System.Windows.Forms.Button();
            this.ButStop = new System.Windows.Forms.Button();
            this.WB = new System.Windows.Forms.WebBrowser();
            this.SuspendLayout();
            // 
            // edURL
            // 
            this.edURL.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.edURL.Location = new System.Drawing.Point(12, 12);
            this.edURL.Name = "edURL";
            this.edURL.Size = new System.Drawing.Size(428, 20);
            this.edURL.TabIndex = 0;
            this.edURL.Text = "https://google.com";
            // 
            // ButGO
            // 
            this.ButGO.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ButGO.BackColor = System.Drawing.Color.Lime;
            this.ButGO.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ButGO.ForeColor = System.Drawing.Color.Cornsilk;
            this.ButGO.Location = new System.Drawing.Point(446, 12);
            this.ButGO.Name = "ButGO";
            this.ButGO.Size = new System.Drawing.Size(33, 20);
            this.ButGO.TabIndex = 1;
            this.ButGO.Text = "GO";
            this.ButGO.UseVisualStyleBackColor = false;
            // 
            // ButBack
            // 
            this.ButBack.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ButBack.BackColor = System.Drawing.Color.Black;
            this.ButBack.ForeColor = System.Drawing.SystemColors.Control;
            this.ButBack.Location = new System.Drawing.Point(209, 419);
            this.ButBack.Name = "ButBack";
            this.ButBack.Size = new System.Drawing.Size(75, 23);
            this.ButBack.TabIndex = 2;
            this.ButBack.Text = "<< Back";
            this.ButBack.UseVisualStyleBackColor = false;
            // 
            // ButForward
            // 
            this.ButForward.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ButForward.BackColor = System.Drawing.Color.White;
            this.ButForward.Location = new System.Drawing.Point(290, 419);
            this.ButForward.Name = "ButForward";
            this.ButForward.Size = new System.Drawing.Size(75, 23);
            this.ButForward.TabIndex = 3;
            this.ButForward.Text = "Forward >>";
            this.ButForward.UseVisualStyleBackColor = false;
            // 
            // ButReload
            // 
            this.ButReload.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ButReload.BackColor = System.Drawing.Color.Blue;
            this.ButReload.ForeColor = System.Drawing.Color.White;
            this.ButReload.Location = new System.Drawing.Point(371, 419);
            this.ButReload.Name = "ButReload";
            this.ButReload.Size = new System.Drawing.Size(51, 23);
            this.ButReload.TabIndex = 4;
            this.ButReload.Text = "Reload";
            this.ButReload.UseVisualStyleBackColor = false;
            // 
            // ButStop
            // 
            this.ButStop.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ButStop.BackColor = System.Drawing.Color.Red;
            this.ButStop.ForeColor = System.Drawing.Color.White;
            this.ButStop.Location = new System.Drawing.Point(428, 419);
            this.ButStop.Name = "ButStop";
            this.ButStop.Size = new System.Drawing.Size(51, 23);
            this.ButStop.TabIndex = 5;
            this.ButStop.Text = "Stop";
            this.ButStop.UseVisualStyleBackColor = false;
            // 
            // WB
            // 
            this.WB.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.WB.Location = new System.Drawing.Point(12, 39);
            this.WB.MinimumSize = new System.Drawing.Size(20, 20);
            this.WB.Name = "WB";
            this.WB.Size = new System.Drawing.Size(467, 374);
            this.WB.TabIndex = 6;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.ClientSize = new System.Drawing.Size(491, 454);
            this.Controls.Add(this.WB);
            this.Controls.Add(this.ButStop);
            this.Controls.Add(this.ButReload);
            this.Controls.Add(this.ButForward);
            this.Controls.Add(this.ButBack);
            this.Controls.Add(this.ButGO);
            this.Controls.Add(this.edURL);
            this.Name = "Form1";
            this.Text = "ProjectWeb";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox edURL;
        private System.Windows.Forms.Button ButGO;
        private System.Windows.Forms.Button ButBack;
        private System.Windows.Forms.Button ButForward;
        private System.Windows.Forms.Button ButReload;
        private System.Windows.Forms.Button ButStop;
        private System.Windows.Forms.WebBrowser WB;
    }
}

