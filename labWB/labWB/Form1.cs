﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labWB
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            ButGO.Click += delegate
            {
                WB.Navigate(edURL.Text);
            };
            ButBack.Click += delegate
            {
                WB.GoBack();
            };
            ButForward.Click += delegate
            {
                WB.GoForward();
            };
            ButReload.Click += delegate
            {
                WB.Refresh();
            };
            ButStop.Click += delegate
            {
                WB.Stop();
            };
            WB.DocumentCompleted += delegate
            {
                edURL.Text = WB.Url.ToString();
            };
            edURL.KeyDown += EdURL_KeyDown;
        }

        private void EdURL_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                WB.Navigate(edURL.Text);
        }
    }
}
