﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestProjectCardValidator
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            string sum = "";
            int ch;
            int chs = 1;
            int j = 0;
            int cht;
            Random r = new Random();
            for (int i = 1; i < 16; i++)
            {
                ch = r.Next(10);
                cht = ch;
                if (i % 2 == 1)
                {
                    cht = cht * 2;
                    if (cht > 9)
                    {
                        cht = cht - 9;
                    }
                }
                sum = Convert.ToString(ch) + sum;
                chs = chs + cht;
            }
            while (chs % 10 != 0)
            {
                chs = chs + 1;
                j++;
            }
            j++;
            sum = sum + Convert.ToString(j);
            Assert.IsTrue(CardCheckLibrary.Tools.Check(sum));
        }
    }
}
