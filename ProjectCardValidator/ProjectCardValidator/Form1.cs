﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjectCardValidator
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            tbNumber.Mask = "000000000000000000000";
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void buCheck_Click(object sender, EventArgs e)
        {
            if (!(rbPhone.Checked))
            {
                if (CardCheckLibrary.Tools.Check(tbNumber.Text))
                {
                    label1.ForeColor = Color.White;
                    label2.ForeColor = Color.White;
                    rbCard.ForeColor = Color.White;
                    rbPhone.ForeColor = Color.White;
                    rbAll.ForeColor = Color.White;
                    this.BackColor = Color.Green;
                    label2.Text = ("Валиден");
                    if (rbCard.Checked)
                    {
                        if (tbNumber.Text[0] == '4')
                        {
                            pbIm.Image = imageList1.Images[0];
                        }
                        if (tbNumber.Text[0] == '5')
                        {
                            pbIm.Image = imageList1.Images[1];
                        }
                        if (tbNumber.Text[0] == '2')
                        {
                            pbIm.Image = imageList1.Images[2];
                        }
                    }

                }
                else
                {
                    label2.Text = ("Не валиден");
                    label1.ForeColor = Color.White;
                    label2.ForeColor = Color.White;
                    rbCard.ForeColor = Color.White;
                    rbPhone.ForeColor = Color.White;
                    rbAll.ForeColor = Color.White;
                    this.BackColor = Color.Red;
                }
            }
            if (rbPhone.Checked)
            {
                if (tbNumber.Text.Length > 5)
                {
                    if ((tbNumber.Text.StartsWith("(96")) | (tbNumber.Text.StartsWith("(903")) | (tbNumber.Text.StartsWith("(905")) | (tbNumber.Text.StartsWith("(906")) | (tbNumber.Text.StartsWith("(909")))
                    {
                        pbIm.Image = imageList1.Images[3];
                    }
                    if ((tbNumber.Text.StartsWith("(91")) | (tbNumber.Text.StartsWith("(98")))
                    {
                        pbIm.Image = imageList1.Images[4];
                    }
                    if ((tbNumber.Text.StartsWith("(92")))
                    {
                        pbIm.Image = imageList1.Images[5];
                    }
                    if ((tbNumber.Text.StartsWith("(900")) | (tbNumber.Text.StartsWith("(901")) | (tbNumber.Text.StartsWith("(902")) | (tbNumber.Text.StartsWith("(904")) | (tbNumber.Text.StartsWith("(908")) | (tbNumber.Text.StartsWith("(950")) | (tbNumber.Text.StartsWith("(952")) | (tbNumber.Text.StartsWith("(958")) | (tbNumber.Text.StartsWith("(977")) | (tbNumber.Text.StartsWith("(99")))
                    {
                        pbIm.Image = imageList1.Images[6];
                    }
                    if (tbNumber.Text.StartsWith("(999") | (tbNumber.Text.StartsWith("(951")) | (tbNumber.Text.StartsWith("(953")))
                    {
                        pbIm.Image = null;
                        label3.Text = "Номер может \nпринадлежать \nнескольким операторам";
                    }
                }
                label2.Text = ("Только оператор");
                label1.ForeColor = Color.Black;
                label2.ForeColor = Color.Black;
                rbCard.ForeColor = Color.Black;
                rbPhone.ForeColor = Color.Black;
                rbAll.ForeColor = Color.Black;
                this.BackColor = Color.Yellow;
            }
        }
        private void tbNumber_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {
            this.BackColor = Color.Red;
            label1.ForeColor = Color.White;
            label2.ForeColor = Color.White;
            rbCard.ForeColor = Color.White;
            rbPhone.ForeColor = Color.White;
            rbAll.ForeColor = Color.White;
            pbIm.Image = null;
            label2.Text = ("Проверьте формат ввода");
        }

        private void tbNumber_TextChanged(object sender, EventArgs e)
        {
            if (!(rbPhone.Checked))
            {
                this.BackColor = Color.White;
                label1.ForeColor = Color.Black;
                label2.ForeColor = Color.Black;
                rbCard.ForeColor = Color.Black;
                rbPhone.ForeColor = Color.Black;
                rbAll.ForeColor = Color.Black;
                pbIm.Image = null;
                label2.Text = ("");

            }
            if (rbPhone.Checked)
            {
                label2.Text = ("Только оператор");
                label1.ForeColor = Color.Black;
                label2.ForeColor = Color.Black;
                rbCard.ForeColor = Color.Black;
                rbPhone.ForeColor = Color.Black;
                rbAll.ForeColor = Color.Black;
                this.BackColor = Color.Yellow;
                pbIm.Image = null;

            }
        }
        private void tbNumber_Enter(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                buCheck.PerformClick();
            }
        }

        private void rbCard_CheckedChanged(object sender, EventArgs e)
        {
            tbNumber.Mask = "0000 0000 0000 000000000";
        }

        private void rbPhone_CheckedChanged(object sender, EventArgs e)
        {
            tbNumber.Mask = "(000) 000-00-00";
            label2.Text = ("Только оператор");
            label1.ForeColor = Color.Black;
            label2.ForeColor = Color.Black;
            rbCard.ForeColor = Color.Black;
            rbPhone.ForeColor = Color.Black;
            rbAll.ForeColor = Color.Black;
            this.BackColor = Color.Yellow;
        }

        private void rbAll_CheckedChanged(object sender, EventArgs e)
        {
            tbNumber.Mask = "000000000000000000000";
        }

    }
}
