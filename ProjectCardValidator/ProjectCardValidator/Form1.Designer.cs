﻿namespace ProjectCardValidator
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.label1 = new System.Windows.Forms.Label();
            this.pbIm = new System.Windows.Forms.PictureBox();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.buCheck = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.tbNumber = new System.Windows.Forms.MaskedTextBox();
            this.rbCard = new System.Windows.Forms.RadioButton();
            this.rbPhone = new System.Windows.Forms.RadioButton();
            this.rbAll = new System.Windows.Forms.RadioButton();
            this.label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pbIm)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "Номер";
            // 
            // pbIm
            // 
            this.pbIm.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.pbIm.Location = new System.Drawing.Point(199, 35);
            this.pbIm.Name = "pbIm";
            this.pbIm.Size = new System.Drawing.Size(160, 116);
            this.pbIm.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbIm.TabIndex = 2;
            this.pbIm.TabStop = false;
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "Visa.jpg");
            this.imageList1.Images.SetKeyName(1, "MC.jpg");
            this.imageList1.Images.SetKeyName(2, "mirnps.jpg");
            this.imageList1.Images.SetKeyName(3, "beeline.png");
            this.imageList1.Images.SetKeyName(4, "1525328909_1.jpg");
            this.imageList1.Images.SetKeyName(5, "share_ru_537x537.png");
            this.imageList1.Images.SetKeyName(6, "1479762952162771400.jpg");
            // 
            // buCheck
            // 
            this.buCheck.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buCheck.Location = new System.Drawing.Point(365, 114);
            this.buCheck.Name = "buCheck";
            this.buCheck.Size = new System.Drawing.Size(111, 32);
            this.buCheck.TabIndex = 3;
            this.buCheck.Text = "Проверить";
            this.buCheck.UseVisualStyleBackColor = true;
            this.buCheck.Click += new System.EventHandler(this.buCheck_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(12, 37);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(0, 25);
            this.label2.TabIndex = 4;
            // 
            // tbNumber
            // 
            this.tbNumber.Location = new System.Drawing.Point(69, 6);
            this.tbNumber.Mask = "0000 0000 0000 000000000";
            this.tbNumber.Name = "tbNumber";
            this.tbNumber.Size = new System.Drawing.Size(376, 22);
            this.tbNumber.TabIndex = 5;
            this.tbNumber.MaskInputRejected += new System.Windows.Forms.MaskInputRejectedEventHandler(this.tbNumber_MaskInputRejected);
            this.tbNumber.TextChanged += new System.EventHandler(this.tbNumber_TextChanged);
            this.tbNumber.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tbNumber_Enter);
            // 
            // rbCard
            // 
            this.rbCard.AutoSize = true;
            this.rbCard.Location = new System.Drawing.Point(12, 71);
            this.rbCard.Name = "rbCard";
            this.rbCard.Size = new System.Drawing.Size(116, 21);
            this.rbCard.TabIndex = 6;
            this.rbCard.TabStop = true;
            this.rbCard.Text = "Номер карты";
            this.rbCard.UseVisualStyleBackColor = true;
            this.rbCard.CheckedChanged += new System.EventHandler(this.rbCard_CheckedChanged);
            // 
            // rbPhone
            // 
            this.rbPhone.AutoSize = true;
            this.rbPhone.Location = new System.Drawing.Point(12, 98);
            this.rbPhone.Name = "rbPhone";
            this.rbPhone.Size = new System.Drawing.Size(142, 21);
            this.rbPhone.TabIndex = 7;
            this.rbPhone.TabStop = true;
            this.rbPhone.Text = "Номер телефона";
            this.rbPhone.UseVisualStyleBackColor = true;
            this.rbPhone.CheckedChanged += new System.EventHandler(this.rbPhone_CheckedChanged);
            // 
            // rbAll
            // 
            this.rbAll.AutoSize = true;
            this.rbAll.Location = new System.Drawing.Point(12, 125);
            this.rbAll.Name = "rbAll";
            this.rbAll.Size = new System.Drawing.Size(118, 21);
            this.rbAll.TabIndex = 8;
            this.rbAll.TabStop = true;
            this.rbAll.Text = "Любой номер";
            this.rbAll.UseVisualStyleBackColor = true;
            this.rbAll.CheckedChanged += new System.EventHandler(this.rbAll_CheckedChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(196, 60);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(169, 51);
            this.label3.TabIndex = 9;
            this.label3.Text = "";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(482, 158);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.rbAll);
            this.Controls.Add(this.rbPhone);
            this.Controls.Add(this.rbCard);
            this.Controls.Add(this.tbNumber);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.buCheck);
            this.Controls.Add(this.pbIm);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(500, 205);
            this.MinimumSize = new System.Drawing.Size(500, 205);
            this.Name = "Form1";
            this.Text = "Валидатор";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pbIm)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pbIm;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.Button buCheck;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.MaskedTextBox tbNumber;
        private System.Windows.Forms.RadioButton rbCard;
        private System.Windows.Forms.RadioButton rbPhone;
        private System.Windows.Forms.RadioButton rbAll;
        private System.Windows.Forms.Label label3;
    }
}

