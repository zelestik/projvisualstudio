﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LabMyNotes
{
    public partial class fmMainMenu : Form
    {
        public fmMainMenu()
        {
            InitializeComponent();
            //
            miCreateNote.Click += MiCreateNote_Click;
            //
            miAboutApp.Click += MiAboutApp_Click;
            //
            miAboutAuthor.Click += MiAboutAuthor_Click;
            //
            miOptions.Click += MiOptions_Click;
            //
            miCreateCalendar.Click += MiCreateCalendar_Click;
        }

        private void MiCreateCalendar_Click(object sender, EventArgs e)
        {
            fmCalendar x = new fmCalendar();
            x.MdiParent = this;
            x.Show();
        }

        private void MiOptions_Click(object sender, EventArgs e)
        {
            fmOptions x = new fmOptions();
            x.ShowDialog();
        }

        private void MiAboutAuthor_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Смиянов Д.А");
        }

        private void MiAboutApp_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Лабораторная работа");
        }

        private void MiCreateNote_Click(object sender, EventArgs e)
        {
            fmNote x = new fmNote();
            x.MdiParent = this;
            x.Show();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
