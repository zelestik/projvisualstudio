﻿namespace LabMyNotes
{
    partial class fmMainMenu
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.заметкаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.miCreate = new System.Windows.Forms.ToolStripMenuItem();
            this.справкаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.miAboutApp = new System.Windows.Forms.ToolStripMenuItem();
            this.miAboutAuthor = new System.Windows.Forms.ToolStripMenuItem();
            this.инструментыToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.miOptions = new System.Windows.Forms.ToolStripMenuItem();
            this.miCreateNote = new System.Windows.Forms.ToolStripMenuItem();
            this.miCreateCalendar = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.заметкаToolStripMenuItem,
            this.инструментыToolStripMenuItem,
            this.справкаToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(599, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // заметкаToolStripMenuItem
            // 
            this.заметкаToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miCreate});
            this.заметкаToolStripMenuItem.Name = "заметкаToolStripMenuItem";
            this.заметкаToolStripMenuItem.Size = new System.Drawing.Size(64, 20);
            this.заметкаToolStripMenuItem.Text = "Заметка";
            // 
            // miCreate
            // 
            this.miCreate.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miCreateNote,
            this.miCreateCalendar});
            this.miCreate.Name = "miCreate";
            this.miCreate.Size = new System.Drawing.Size(152, 22);
            this.miCreate.Text = "Создать";
            // 
            // справкаToolStripMenuItem
            // 
            this.справкаToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miAboutApp,
            this.miAboutAuthor});
            this.справкаToolStripMenuItem.Name = "справкаToolStripMenuItem";
            this.справкаToolStripMenuItem.Size = new System.Drawing.Size(65, 20);
            this.справкаToolStripMenuItem.Text = "Справка";
            // 
            // miAboutApp
            // 
            this.miAboutApp.Name = "miAboutApp";
            this.miAboutApp.Size = new System.Drawing.Size(152, 22);
            this.miAboutApp.Text = "О программе";
            // 
            // miAboutAuthor
            // 
            this.miAboutAuthor.Name = "miAboutAuthor";
            this.miAboutAuthor.Size = new System.Drawing.Size(152, 22);
            this.miAboutAuthor.Text = "Об авторе";
            // 
            // инструментыToolStripMenuItem
            // 
            this.инструментыToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miOptions});
            this.инструментыToolStripMenuItem.Name = "инструментыToolStripMenuItem";
            this.инструментыToolStripMenuItem.Size = new System.Drawing.Size(95, 20);
            this.инструментыToolStripMenuItem.Text = "Инструменты";
            // 
            // miOptions
            // 
            this.miOptions.Name = "miOptions";
            this.miOptions.Size = new System.Drawing.Size(152, 22);
            this.miOptions.Text = "Настройки";
            // 
            // miCreateNote
            // 
            this.miCreateNote.Name = "miCreateNote";
            this.miCreateNote.Size = new System.Drawing.Size(152, 22);
            this.miCreateNote.Text = "Заметку";
            // 
            // miCreateCalendar
            // 
            this.miCreateCalendar.Name = "miCreateCalendar";
            this.miCreateCalendar.Size = new System.Drawing.Size(152, 22);
            this.miCreateCalendar.Text = "Календарь";
            // 
            // fmMainMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(599, 349);
            this.Controls.Add(this.menuStrip1);
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "fmMainMenu";
            this.Text = "Мои заметки";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem заметкаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem miCreate;
        private System.Windows.Forms.ToolStripMenuItem справкаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem miAboutApp;
        private System.Windows.Forms.ToolStripMenuItem miAboutAuthor;
        private System.Windows.Forms.ToolStripMenuItem инструментыToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem miOptions;
        private System.Windows.Forms.ToolStripMenuItem miCreateNote;
        private System.Windows.Forms.ToolStripMenuItem miCreateCalendar;
    }
}

