﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using CalcLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalcLibrary.Tests
{
    [TestClass()]
    public class UtilsTests
    {
        [TestMethod()]
        public void SumTest()
        {
            int a = 1;
            int b = 2;
            int c = 3;
            int r = 6;
            //
            int rr = Utils.Sum(a, b, c);
            //
            Assert.AreEqual(r, rr);
        }

        [TestMethod()]
        public void MinTest()
        {
            int a = 4;
            int b = 90;
            int r = 4;
            //
            int rr = Utils.Min(a, b);
            //
            Assert.AreEqual(r, rr);
        }
        [TestMethod()]
        public void MinTest2()
        {
            int a = -3;
            int b = -2;
            int r = -3;
            //
            int rr = Utils.Min(a, b);
            //
            Assert.AreEqual(r, rr);
        }
        [TestMethod()]
        public void AveTest()
        {
            int a = 4;
            int b = 10;
            int c = 7;
            int r = 7;
            //
            int rr = Utils.Ave(a, b, c);
            //
            Assert.AreEqual(r, rr);
        }
        [TestMethod()]
        public void MulTest()
        {
            int a = 4;
            int b = 90;
            int r = 360;
            //
            int rr = Utils.Mul(a, b);
            //
            Assert.AreEqual(r, rr);
        }
    }
}