﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalcLibrary
{
    public static class Utils
    {
        public static int Sum (int a, int b, int c)
        {
            return a + b + c;
        }

        public static int Min (int a, int b)
        {
            return (a > b) ? b : a;
        }
        public static int Ave(int a, int b, int c)
        {
            return (a + b + c) / 3;
        }
        public static int Mul(int a, int b)
        {
            return a * b;
        }
    }
}
