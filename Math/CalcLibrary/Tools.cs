﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalcLibrary
{
    public static class Tools
    {
        public static int Formula(int a, int b, int c)
        {
            return b*b - 4*a*c;
        }
    }
}
