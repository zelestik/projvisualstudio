﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace labGenPasswordCore
{
    public class Utils
    {
        public static string RandomStr(int aLenght, bool aLower, bool aUpper, bool aNumbr, bool aSpec)
        {
            string c1 = "abcdefghijklmnopgrstuvwxyz";
            string c2 = "0123456789";
            string c3 = "[]{}<>,.;:-+#";
            //
            var x = new StringBuilder(); // набор символов
            var xResult = new StringBuilder();
            Random rnd = new Random();
            // Создаём набор символов в соотвествии с параметрами функции
            if (aLower) x.Append(c1);
            if (aUpper) x.Append(c1.ToUpper());
            if (aNumbr) x.Append(c2);
            if (aSpec) x.Append(c3);
            // Если набор остался пустым, то по умолчанию включаем символы нижнего регистра
            if (x.ToString() == "") x.Append(c1);
            //
            while (xResult.Length < aLenght)
                xResult.Append(x[rnd.Next(x.Length)]);
            //
            return xResult.ToString();
        }


    }
}
