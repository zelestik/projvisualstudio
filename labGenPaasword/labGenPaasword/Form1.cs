﻿using labGenPasswordCore;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labGenPaasword
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            buPassword.Click += BuPassword_Click;
        }

        private void BuPassword_Click(object sender, EventArgs e)
        {
            edPassword.Text = Utils.RandomStr((int)edLength.Value, ckLower.Checked, ckUpper.Checked, ckNumber.Checked, ckSpec.Checked);
        }

        private void buPassword_Click_1(object sender, EventArgs e)
        {

        }
    }
}
