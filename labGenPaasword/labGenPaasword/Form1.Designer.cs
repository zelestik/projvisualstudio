﻿namespace labGenPaasword
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.edPassword = new System.Windows.Forms.TextBox();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.buPassword = new System.Windows.Forms.Button();
            this.ckLower = new System.Windows.Forms.CheckBox();
            this.ckUpper = new System.Windows.Forms.CheckBox();
            this.ckNumber = new System.Windows.Forms.CheckBox();
            this.ckSpec = new System.Windows.Forms.CheckBox();
            this.Lk = new System.Windows.Forms.Label();
            this.edLength = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.edLength)).BeginInit();
            this.SuspendLayout();
            // 
            // edPassword
            // 
            this.edPassword.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.edPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.edPassword.ForeColor = System.Drawing.Color.Green;
            this.edPassword.Location = new System.Drawing.Point(16, 15);
            this.edPassword.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.edPassword.Name = "edPassword";
            this.edPassword.Size = new System.Drawing.Size(345, 45);
            this.edPassword.TabIndex = 0;
            this.edPassword.Text = "<Password>";
            this.edPassword.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Font = new System.Drawing.Font("Segoe UI", 20F);
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // buPassword
            // 
            this.buPassword.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buPassword.ForeColor = System.Drawing.Color.Green;
            this.buPassword.Location = new System.Drawing.Point(16, 86);
            this.buPassword.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.buPassword.Name = "buPassword";
            this.buPassword.Size = new System.Drawing.Size(347, 74);
            this.buPassword.TabIndex = 2;
            this.buPassword.Text = "Сгенерировать";
            this.buPassword.UseVisualStyleBackColor = true;
            this.buPassword.Click += new System.EventHandler(this.buPassword_Click_1);
            // 
            // ckLower
            // 
            this.ckLower.AutoSize = true;
            this.ckLower.ForeColor = System.Drawing.Color.Green;
            this.ckLower.Location = new System.Drawing.Point(16, 169);
            this.ckLower.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ckLower.Name = "ckLower";
            this.ckLower.Size = new System.Drawing.Size(217, 21);
            this.ckLower.TabIndex = 3;
            this.ckLower.Text = "Символы в нижнем регистре";
            this.ckLower.UseVisualStyleBackColor = true;
            // 
            // ckUpper
            // 
            this.ckUpper.AutoSize = true;
            this.ckUpper.ForeColor = System.Drawing.Color.Green;
            this.ckUpper.Location = new System.Drawing.Point(16, 198);
            this.ckUpper.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ckUpper.Name = "ckUpper";
            this.ckUpper.Size = new System.Drawing.Size(221, 21);
            this.ckUpper.TabIndex = 4;
            this.ckUpper.Text = "Символы в верхнем регистре";
            this.ckUpper.UseVisualStyleBackColor = true;
            // 
            // ckNumber
            // 
            this.ckNumber.AutoSize = true;
            this.ckNumber.ForeColor = System.Drawing.Color.Green;
            this.ckNumber.Location = new System.Drawing.Point(16, 226);
            this.ckNumber.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ckNumber.Name = "ckNumber";
            this.ckNumber.Size = new System.Drawing.Size(78, 21);
            this.ckNumber.TabIndex = 5;
            this.ckNumber.Text = "Цифры";
            this.ckNumber.UseVisualStyleBackColor = true;
            // 
            // ckSpec
            // 
            this.ckSpec.AutoSize = true;
            this.ckSpec.ForeColor = System.Drawing.Color.Green;
            this.ckSpec.Location = new System.Drawing.Point(16, 255);
            this.ckSpec.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ckSpec.Name = "ckSpec";
            this.ckSpec.Size = new System.Drawing.Size(181, 21);
            this.ckSpec.TabIndex = 6;
            this.ckSpec.Text = "Специальные символы";
            this.ckSpec.UseVisualStyleBackColor = true;
            // 
            // Lk
            // 
            this.Lk.AutoSize = true;
            this.Lk.ForeColor = System.Drawing.Color.Green;
            this.Lk.Location = new System.Drawing.Point(16, 284);
            this.Lk.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Lk.Name = "Lk";
            this.Lk.Size = new System.Drawing.Size(103, 17);
            this.Lk.TabIndex = 7;
            this.Lk.Text = "Длина пароля";
            // 
            // edLength
            // 
            this.edLength.Location = new System.Drawing.Point(129, 282);
            this.edLength.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.edLength.Name = "edLength";
            this.edLength.Size = new System.Drawing.Size(160, 22);
            this.edLength.TabIndex = 8;
            this.edLength.Value = new decimal(new int[] {
            6,
            0,
            0,
            0});
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.GreenYellow;
            this.ClientSize = new System.Drawing.Size(379, 321);
            this.Controls.Add(this.edLength);
            this.Controls.Add(this.Lk);
            this.Controls.Add(this.ckSpec);
            this.Controls.Add(this.ckNumber);
            this.Controls.Add(this.ckUpper);
            this.Controls.Add(this.ckLower);
            this.Controls.Add(this.buPassword);
            this.Controls.Add(this.edPassword);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "Form1";
            this.Text = "Генератор паролей";
            ((System.ComponentModel.ISupportInitialize)(this.edLength)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox edPassword;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.Button buPassword;
        private System.Windows.Forms.CheckBox ckLower;
        private System.Windows.Forms.CheckBox ckUpper;
        private System.Windows.Forms.CheckBox ckNumber;
        private System.Windows.Forms.CheckBox ckSpec;
        private System.Windows.Forms.Label Lk;
        private System.Windows.Forms.NumericUpDown edLength;
    }
}

