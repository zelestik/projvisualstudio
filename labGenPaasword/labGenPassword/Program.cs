﻿using labGenPasswordCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace labGenPassword
{
    class Program
    {
        static void Main(string[] args)
        {
            int l;
            string lw;
            string hg;
            string num;
            string sp;
            bool lwt;
            bool hgt;
            bool numt;
            bool spt;
            lwt = false;
            hgt = false;
            numt = false;
            spt = false;
            Console.WriteLine("Введите длинну пароля");
            l = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Символы нижнего регистра (Y/N)");
            lw = Convert.ToString(Console.ReadLine());
            if (lw == "Y") lwt = true;
            Console.WriteLine("Символы верхнего регистра (Y/N)");
            hg = Convert.ToString(Console.ReadLine());
            if (hg == "Y") hgt = true;
            Console.WriteLine("Цифры (Y/N)");
            num = Convert.ToString(Console.ReadLine());
            if (num == "Y") numt = true;
            Console.WriteLine("Спец. символы (Y/N)");
            sp = Convert.ToString(Console.ReadLine());
            if (sp == "Y") spt = true;
            Console.WriteLine(Utils.RandomStr((int)l, lwt, hgt, numt, spt));
            Console.ReadKey();
        }
    }
}
