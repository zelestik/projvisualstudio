﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labPoesia
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            //
            meThroughLine.Lines = UtilsStr.ThroughLine(meText.Lines);
            meFirstWords.Lines = UtilsStr.FirstWords(meText.Lines);
            meThroughLetter.Lines = UtilsStr.ThroughLetter(meText.Lines);
            //
            buZoomOut.Click += BuZoomOut_Click;
            buZoomIn.Click += BuZoomIn_Click;
            buAbout.Click += BuAbout_Click;
        }

        private void BuAbout_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Автор - Смиянов Денис Александрович");
        }

        private void BuZoomIn_Click(object sender, EventArgs e)
        {
            float x = meText.Font.Size;
            x += 1;
            meText.Font = new Font(meText.Font.FontFamily, x);
            meThroughLine.Font = new Font(meText.Font.FontFamily, x);
            meFirstWords.Font = new Font(meFirstWords.Font.FontFamily, x);
            meThroughLetter.Font = new Font(meThroughLetter.Font.FontFamily, x);
        }

        private void BuZoomOut_Click(object sender, EventArgs e)
        {
            float x = meText.Font.Size;
            x -= 1;
            meText.Font = new Font(meText.Font.FontFamily, x);
            meThroughLine.Font = new Font(meText.Font.FontFamily, x);
            meFirstWords.Font = new Font(meFirstWords.Font.FontFamily, x);
            meThroughLetter.Font = new Font(meThroughLetter.Font.FontFamily, x);
        }

        private void meThroughLine_TextChanged(object sender, EventArgs e)
        {

        }

        private void buZoomOut_Click_1(object sender, EventArgs e)
        {

        }
    }
}
