﻿namespace labPoesia
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.buZoomOut = new System.Windows.Forms.ToolStripButton();
            this.buZoomIn = new System.Windows.Forms.ToolStripButton();
            this.buAbout = new System.Windows.Forms.ToolStripButton();
            this.tc = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.meText = new System.Windows.Forms.TextBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.meThroughLine = new System.Windows.Forms.TextBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.meFirstWords = new System.Windows.Forms.TextBox();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.meThroughLetter = new System.Windows.Forms.TextBox();
            this.toolStrip1.SuspendLayout();
            this.tc.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.buZoomOut,
            this.buZoomIn,
            this.buAbout});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(581, 27);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // buZoomOut
            // 
            this.buZoomOut.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.buZoomOut.Image = ((System.Drawing.Image)(resources.GetObject("buZoomOut.Image")));
            this.buZoomOut.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buZoomOut.Name = "buZoomOut";
            this.buZoomOut.Size = new System.Drawing.Size(81, 24);
            this.buZoomOut.Text = "Zoom Out";
            this.buZoomOut.Click += new System.EventHandler(this.buZoomOut_Click_1);
            // 
            // buZoomIn
            // 
            this.buZoomIn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.buZoomIn.Image = ((System.Drawing.Image)(resources.GetObject("buZoomIn.Image")));
            this.buZoomIn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buZoomIn.Name = "buZoomIn";
            this.buZoomIn.Size = new System.Drawing.Size(69, 24);
            this.buZoomIn.Text = "Zoom In";
            // 
            // buAbout
            // 
            this.buAbout.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.buAbout.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.buAbout.Image = ((System.Drawing.Image)(resources.GetObject("buAbout.Image")));
            this.buAbout.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buAbout.Name = "buAbout";
            this.buAbout.Size = new System.Drawing.Size(54, 24);
            this.buAbout.Text = "About";
            // 
            // tc
            // 
            this.tc.Controls.Add(this.tabPage1);
            this.tc.Controls.Add(this.tabPage2);
            this.tc.Controls.Add(this.tabPage3);
            this.tc.Controls.Add(this.tabPage4);
            this.tc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tc.Location = new System.Drawing.Point(0, 27);
            this.tc.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tc.Name = "tc";
            this.tc.SelectedIndex = 0;
            this.tc.Size = new System.Drawing.Size(581, 521);
            this.tc.TabIndex = 1;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.meText);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabPage1.Size = new System.Drawing.Size(573, 492);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Полностью";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // meText
            // 
            this.meText.Dock = System.Windows.Forms.DockStyle.Fill;
            this.meText.Location = new System.Drawing.Point(4, 4);
            this.meText.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.meText.Multiline = true;
            this.meText.Name = "meText";
            this.meText.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.meText.Size = new System.Drawing.Size(565, 484);
            this.meText.TabIndex = 0;
            this.meText.Text = resources.GetString("meText.Text");
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.meThroughLine);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabPage2.Size = new System.Drawing.Size(573, 488);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Через строчку";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // meThroughLine
            // 
            this.meThroughLine.Dock = System.Windows.Forms.DockStyle.Fill;
            this.meThroughLine.Location = new System.Drawing.Point(4, 4);
            this.meThroughLine.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.meThroughLine.Multiline = true;
            this.meThroughLine.Name = "meThroughLine";
            this.meThroughLine.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.meThroughLine.Size = new System.Drawing.Size(565, 480);
            this.meThroughLine.TabIndex = 0;
            this.meThroughLine.TextChanged += new System.EventHandler(this.meThroughLine_TextChanged);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.meFirstWords);
            this.tabPage3.Location = new System.Drawing.Point(4, 25);
            this.tabPage3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabPage3.Size = new System.Drawing.Size(573, 488);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Первые слова";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // meFirstWords
            // 
            this.meFirstWords.Dock = System.Windows.Forms.DockStyle.Fill;
            this.meFirstWords.Location = new System.Drawing.Point(4, 4);
            this.meFirstWords.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.meFirstWords.Multiline = true;
            this.meFirstWords.Name = "meFirstWords";
            this.meFirstWords.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.meFirstWords.Size = new System.Drawing.Size(565, 480);
            this.meFirstWords.TabIndex = 0;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.meThroughLetter);
            this.tabPage4.Location = new System.Drawing.Point(4, 25);
            this.tabPage4.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabPage4.Size = new System.Drawing.Size(573, 488);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Через букву";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // meThroughLetter
            // 
            this.meThroughLetter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.meThroughLetter.Location = new System.Drawing.Point(4, 4);
            this.meThroughLetter.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.meThroughLetter.Multiline = true;
            this.meThroughLetter.Name = "meThroughLetter";
            this.meThroughLetter.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.meThroughLetter.Size = new System.Drawing.Size(565, 480);
            this.meThroughLetter.TabIndex = 0;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(581, 548);
            this.Controls.Add(this.tc);
            this.Controls.Add(this.toolStrip1);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "Form1";
            this.Text = "Учим стих";
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.tc.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton buZoomOut;
        private System.Windows.Forms.ToolStripButton buZoomIn;
        private System.Windows.Forms.ToolStripButton buAbout;
        private System.Windows.Forms.TabControl tc;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TextBox meText;
        private System.Windows.Forms.TextBox meThroughLine;
        private System.Windows.Forms.TextBox meFirstWords;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TextBox meThroughLetter;
    }
}

