﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labMyTest
{
    public partial class TestForm : Form
    {
        int i = 0;
        int[] ans = { 1, 1, 2, 4 };
        public TestForm()
        {
            InitializeComponent();
            pictureBox1.Image = imageList1.Images[i];
            rb1.Text = bu1[i];
            rb2.Text = bu2[i];
            rb3.Text = bu3[i];
            rb4.Text = bu4[i];


        }
        int a = 0;
        int score = 0;
        string[] bu1 = { "Москва", "Лас-Вегас", "Подольск", "Саратов" };
        string[] bu2 = { "Санкт-Петербург", "Мурманск", "Нью-Йорк", "Казань" };
        string[] bu3 = { "Детройт", "Париж", "Саранск", "Владивосток" };
        string[] bu4 = { "Саратов", "Саранск", "Лас-Вегас", "Санкт-Петербург" };
        private void buNext_Click(object sender, EventArgs e)
        {
            if (a == ans[i])
            {
                score = score + 1;
            }
            i = i + 1;
            if (i <= 3)
            {
                pictureBox1.Image = imageList1.Images[i];
                rb1.Text = bu1[i];
                rb2.Text = bu2[i];
                rb3.Text = bu3[i];
                rb4.Text = bu4[i];
            }
            else
            {
                MessageBox.Show("Ваш счёт = " + Convert.ToString(score));
            }

        }

        private void rb1_CheckedChanged(object sender, EventArgs e)
        {
            a = 1;
        }
        private void rb2_CheckedChanged(object sender, EventArgs e)
        {
            a = 2;
        }
        private void rb3_CheckedChanged(object sender, EventArgs e)
        {
            a = 3;
        }
        private void rb4_CheckedChanged(object sender, EventArgs e)
        {
            a = 4;
        }
    }
}
